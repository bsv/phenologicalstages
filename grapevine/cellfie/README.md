have a look to the wiki for the documentation

# Cellfie transformation rules

fix the renderer of protege with entity IRI short name

All the files enable to create the grapevine_scales_latest.owl file.

grapevine_scales_vide.owl is the entry file used during cellfie transformation rules in order to produce grapevine_scales_latest.owl.
The algorithm is detailled in the wiki PhenologicalStages

grapevine_bbch_principalStages.xlsx contains the information about principal stages of BBCH individual scale of grapevine.
grapevine_bbch_secondaryStages.xlsx contains the information about secondary stages of BBCH individual scale of grapevine.
grapevine_baggiolini_secondaryStages.xlsx contains the information about secondary stages of baggiolini scale of grapevine.
grapevine_el_secondaryStages.xlsx contains the information about secondary stages of EL scale of grapevine.
grapevine_epicure_secondaryStages.xlsx contains the information about secondary stages of IFV Epicure scale of grapevine.
grapevine_ifv_secondaryStages.xlsx contains the information about secondary stages of IFV labels scale of grapevine.

First create all the individuals using the rule pattern:
rulesInstancesGeneration_grapevine_XXX.json are the cellfie rules to generate stages. XXX is the name of the scale.
It takes as input grapevine_XXX_stages.xlsx file.
The generation should start with BBCH principal stage and should be applied on the 6 slsx files.

Second create the alignements
rulesLinksGeneration_grapevine_XXX_Stages.json is the cellfie rules to generate the alignements from the stages of XXX scale. 
It takes as input  grapevine_XXX_stages.xlsx file.

Third inferes the inverse property links using SWRL engine
table 5 select PROP_INV1 and PROP_INV2




