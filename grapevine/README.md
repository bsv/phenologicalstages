# Plant Phenological Development knowledge graph dedicated to GrapeVine (PPD-GV)

The Knowledge Graph about Plant Phenological Development dedicated to GrapeVine (PPD-GV) contains the description of several phenological scales : 
* BBCH scale of grapevine , 
* Einhornn and Lorenz (EL) scale, 
* Baggiolini scale, 
* IFV labels scale : IFV labels is a kind of BBCH stages selection. It was produced by IFV. 
* Epicure : Epicure is the name of the database that store all grapevine observations. This database has created an extension of EL scale. Epicure scale and database was produced by IFV.

The KG contains also the alignment between the 5 scales using skos mapping properties. Those alignement was validated by two domain experts from IFV: Xavier DELPUECH and Marc RAYNAL.
The two PDF files untitled grapevineScalesMapping present those alignements.

This KG is an output of D2KAB project task 4.3. The grapevine scales publication works was made under the supervision of Xavier DELPUECH from French Institute of Grapevine and Vine (IFV)

## Access Description and Identifier

The URI of PPD-GV is https://opendata.inrae.fr/ppd-res/grapevine# 

PPD-GV is under license https://creativecommons.org/licenses/by-nd/4.0/

PPD-GV is available on :
 * AgroPortal  https://AgroPortal.lirmm.fr/ontologies/PPD-GV
 * Research Data Gouv: ROUSSEY, Catherine; DELPUECH, Xavier; RAYNAL, Marc, 2024, "Phenological Scales of Grapevine", https://doi.org/10.57745/YVM7VH, Recherche Data Gouv, V1
 * SPARQL end point https://rdf.codex.cati.inrae.fr/ppd-res/grapevine/sparql


## Development Method
The KG populates the ontology ppdo using the stages of 5 scales.
The population process was made using Protégé Cellfie plugin.
All the files used during the generation of  grapevine_scales.owl are stored in cellfie directory.

## Sources

* Growth stages of mono- and dicotyledonous plants BBCH Monograph edited by Uwe Meier Julius Kühn-Institut (JKI) Quedlinburg 2018 
DOI: 10.5073/20180906-074619
ISBN: 978-3-95547-071-5
* Stades phénologiques des mono-et dicotylédones cultivées BBCH
Monographie rédigé par Uwe Meier Instituto Julius Kühn (JKI, siglas en alomán) Quedlinburg, 2018 
DOI: 10.5073/20180906-075455
ISBN: 978-3-95547-072-2
* Estadios de las plantas mono-y dicotyledóneas BBCH Monografia Elaborado por Uwe Meier Instituto Julius Kühn (JKI, siglas en alomán) Quedlinburg, 2018 
DOI: 10.5073/20180906-075743
ISBN: 978-3-95547-069-2
* Entwicklungsstadien mono- und dikotyler Pflanzen BBCH Monografie bearbeitet von Uwe Meier Julius Kühn-Institut (JKI) Quedlinburg 2018 DOI: 10.5073/20180906-075119
ISBN: 978-3-95547-070-8
* BAGGIOLINI, M. Les stades repères dans le développement de la vigne et leur utilisation pratique. Revue Romande d'Agriculture, de Viticulture, d'Arboriculture, 1952, Vol 8, N°1, pages 4-6.
* BAILLOD, M. et BAGGIOLINI, M. Les stades repères de la vigne. Revue Suisse de Viticulture, Arboriculture, Horticulture, 1993, vol. 25.
* DENIZOT, Anne-Marie. Les stades phénologiques de la vigne. 2019, IFV, https://www.vignevin.com/wp-content/uploads/2019/05/Poster-stades-ph%C3%A9nologiques-de-la-vigne.pdf, credit photo P Mackiewicz IFV Octobre 2017
* DENIZOT, Anne-Marie. Les stades phénologiques de la vigne. 2018, IFV, https://www.vignevin-occitanie.com/wp-content/uploads/2018/11/PosterStadPhen091.pdf, credit photo P Mackiewicz AM Denizot IFV Janvier 2009
* EICHHORN, K. W., LORENZ, D. H., et al. Phenological development stages of the grape vine. Nachrichtenblatt des deutschen Pflanzenschutzdienstes, 1977, vol. 29, no 8, p. 119-120.
* LORENZ, D. H., EICHHORN, K. W., BLEIHOLDER, H., et al. Growth Stages of the Grapevine: Phenological growth stages of the grapevine (Vitis vinifera L. ssp. vinifera)—Codes and descriptions according to the extended BBCH scale. Australian Journal of Grape and Wine Research, 1995, vol. 1, no 2, p. 100-103.

## Contributors 
* Stephan BERNARD, LISC, INRAE (https://orcid.org/0000-0001-9694-1443): he publishes on the web the phenological scales per crops and takes care of the associated SPARQL endpoints.
* Christian DEBORD, IFV: he updates the IFV-Epicure scale and their alignements in the EPICURE database and associated information system.
* Xavier DELPUECH, IFV, (https://orcid.org/0000-0001-7278-3209): he provides some sources for grapevine phenological scales. He proposes some alignements between grapevine scales.
* Marc RAYNAL, IFV: he provides the sources for grapevine phenological scales. He proposes some alignements between grapevine scales.
* Catherine ROUSSEY, MISTEA, INRAE (https://orcid.org/0000-0002-3076-5499): She creates the OWL files for grapevine scales using Protege tool and cellfie plugin. She helps the experts to provide correct alignements. She updates the description of grapevine scales in Agroportal repository.
* Marc VERGNES, IFV: He proposes some alignements between grapevine scales.

Productors:
* National research institute for agriculture food and environment (INRAE) [https://www.inrae.fr/], ror:[https://ror.org/003vg9w96]
* Mathématiques, Informatique et Statistique pour l'Environnement et l'Agronomie (MISTEA) [https://www6.montpellier.inra.fr/mistea_eng/], ror: [https://ror.org/01pd2sz18]
* Institut Français de la Vigne et du Vin (IFV) [http://www.vignevin.com/], ror: [https://ror.org/03synvw58]

Funding:
* ANR-18-CE23-0017 D2KAB (Data to Knowledge in Agronomy and Biodiversity) http://www.d2kab.org . WP T4.3 BSV Reader

# Documentation

Grapevine scales are described in two publications:
* C. ROUSSEY, X. DELPUECH, F. AMARDEILH, S. BERNARD, C. JONQUET. Semantic Description of Plant Phenological Development Stages, starting with Grapevine. In Proceedings of 14th international conference on Metadata and Semantics Research Conference (MTSR), Madrid, Spain, 2-4 December 2020.   In: Garoufallou E., Ovalle-Perandones MA. (eds) Metadata and Semantic Research. MTSR 2020. Communications in Computer and Information Science (CCIS), vol 1355. Springer, Cham, p 257-268.  https://dx.doi.org/10.1007/978-3-030-71903-6_25, https://hal.inrae.fr/hal-02996846, OA 
* C. ROUSSEY,  X. DELPUECH, M. RAYNAL, F. AMARDEILH, S. BERNARD, C. JONQUET, C. NOÛS. Description sémantique des stades de développement phénologique des plantes, cas d’étude de la vigne. Dans les Actes des 32e  Journées Francophones d'Ingénierie des Connaissances IC 2021, adossée à la 14e Plate-forme Francophone d'Intelligence Artificielle, 28 juin - 2 juillet 2021, Bordeaux, France. https://hal.inrae.fr/emse-03260085, OA

# Git content description

grapevine_scales_latest.owl is the last version of grapevine scale in OWL/XML format to use with Protégé.
grapevine_scales_latest.rdf is the RDF-XML format handled by Skos Play tool.
Note that in Skos Play tool can not handle one file containning several concept schemes: Hierarchical redundancy between concepts belonging to distinct concept schemes are detected, same error for labels.



grapevineScalesMappings_1 et _2 files are mappings schema between the 5 scales.

Cellfie folder contains all the files used by cellfie plugin.
