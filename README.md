# BBCH-based Plant Phenological Description Ontology (PPDO) and generic scales (PPD-GEN)

This repository is dedicated to the construction of  semantic resources (several extended SKOS models and one OWL ontology) that represent plant phenological development stages for agricultural practices. This work is part of the ANR D2KAB project. 

BBCH-based Plant Phenological Description Ontology (PPDO) is an ontology that extends skos model to store BBCH phenological scales and other scales. A phenological scale is a skos:Concept Scheme. All the BBCH stages that have the same identifier (for example BBCH 01) belongs to the same class.

PPDO is used to trstucture two BBCH generic scales:
*  the BBCH general scale that could be used for any crop that do not have an BBCH individual scale.
*  the BBCH global scale, a new scale created for the purpose of D2KAB project. This scale contains all the phenological stages from the BBCH general scale and all BBCH individual scales. 
This two generic scales compose a new knowledge graph (PPD-GEN)
Others BBCH individual scales are availables in specific folder in this git repository.

## Access Description and Identifier

PPDO is published on the web at the uri https://opendata.inrae.fr/ppd-def#

The licence of PPDO is Attribution-NoDerivatives 4.0 International (CC BY-ND 4.0)  https://creativecommons.org/licenses/by-nd/4.0/

PPDO is available on :
 * AgroPortal https://AgroPortal.lirmm.fr/ontologies/PPDO 
 * Research Data Gouv: ROUSSEY, Catherine; AMARDEILH, Florence; DELPUECH, Xavier; RAYNAL, Marc; JONQUET, Clement, 2021, "BBCH-based Plant Phenological Description Ontology", https://doi.org/10.15454/TIMQHW, Recherche Data Gouv, V1
 * SPARQL end point https://rdf.codex.cati.inrae.fr/ppd-def/sparql
 
PPD-GEN is published on the web at the uri https://opendata.inrae.fr/ppd-res/generic#

PPD-GEN is under licence is under licence https://creativecommons.org/licenses/by-nd/4.0/
If you want to modify this KG please contact Catherine ROUSSEY <catherine.roussey@inrae.fr>

PPD-GEN is available on :
 * AgroPortal   https://AgroPortal.lirmm.fr/ontologies/PPD-GEN 
 * SPARQL end https://rdf.codex.cati.inrae.fr/ppd-res/generic/sparql

## Specifications
* What are the scales associated to a specific crop?
* What is the name of a specific scale and its document source?
* How many stages belong to a specific scale?
* What are the development stages of a specific crop?
* What are the primary and secondary stages of a specific BBCH scale?
* What are the labels and definitions (in french, english, spanish and german) of a specific stage?
* What are the equivalent stages belonging to various scales dedicated to a specific crop?
* What are the stages of a specific scale that are aligned with stages of another scale?
* What are the stage that appears just before (or after) a specific stage in a specific scale?
* What are the order of the stages that belong to the same scale?

## Development Method
The ontology engineering process used the Linked Open Terminology (LOT) methodology.

The engineering process was made using Protégé Cellfie plugin.
All the files used during the generation of ppdo.owl are stored in Cellfie directory.

## Sources

* Growth stages of mono- and dicotyledonous plants BBCH Monograph edited by Uwe Meier Julius Kühn-Institut (JKI) Quedlinburg 2018 
DOI: 10.5073/20180906-074619
ISBN: 978-3-95547-071-5
* Stades phénologiques des mono-et dicotylédones cultivées BBCH
Monographie rédigé par Uwe Meier Instituto Julius Kühn (JKI, siglas en alomán) Quedlinburg, 2018 
DOI: 10.5073/20180906-075455
ISBN: 978-3-95547-072-2
* Estadios de las plantas mono-y dicotyledóneas BBCH Monografia Elaborado por Uwe Meier Instituto Julius Kühn (JKI, siglas en alomán) Quedlinburg, 2018 
DOI: 10.5073/20180906-075743
ISBN: 978-3-95547-069-2
* Entwicklungsstadien mono- und dikotyler Pflanzen BBCH Monografie bearbeitet von Uwe Meier Julius Kühn-Institut (JKI) Quedlinburg 2018 DOI: 10.5073/20180906-075119
ISBN: 978-3-95547-070-8

## Contributors
* Florence AMARDEILH, ELZEARD, (https://orcid.org/0000-0002-6306-4437): She validates the conceptualization of PPDO. She builds the XLSX files for BBCH phenological scales: leaf vegetables forming heads, root and stem vegetables, solanaceous fruits.
* Sophie AUBIN, DIPSO, INRAE (https://orcid.org/0000-0003-4805-8220): she provides some recommendations on semantic ressource publication on the Web using Agroportal repository and Research Data Gouv portal.
* Stephan BERNARD, LISC, INRAE (https://orcid.org/0000-0001-9694-1443): he publishes on the web the phenological scales per crops and takes care of the associated SPARQL endpoints.
* Robert BOSSY, MAIAGE, INRAE (https://orcid.org/0000-0001-6652-9319): he builds the NLP framework Alvis NLP. He provides some recommendations on scale projection on bulletins.
* Remi CERES, Elzeard, (https://orcid.org/0009-0003-8946-4601): he takes part of the discussion about the PPDO conceptualization.
* Anna CHEPAIKINA, TSCF, MAIAGE, INRAE (https://orcid.org/0000-0001-7494-7748): she proposes a NLP workflow to project stage labels on French Alert Bulletins. So she takes part of the validation of stage labels.
* Marine COURTIN, MAIAGE, INRAE (https://orcid.org/0000-0003-4189-4322): she proposes a NLP workflow to project stage labels on French Alert Bulletins. So she takes part of the validation of stage labels.
* Xavier DELPUECH, IFV, (https://orcid.org/0000-0001-7278-3209): he provides some sources for grapevine phenological scales. He proposes some alignements between grapevine scales.
* Matthieu HIRSCHY, ACTA (https://orcid.org/0000-0002-2711-7714): he builds the XLSX files for BBCH individual phenological scale: cereals.
* Clement JONQUET, MISTEA, INRAE (https://orcid.org/0000-0002-2404-1582): he discuss and validates the PPDO conceptualization and provides some recommendations on semantic ressource publication on the Web using Agroportal repository.
* Franck MICHEL, INRIA Sophia Antipolis (https://orcid.org/0000-0001-9064-0463): he provides some recommendations on KG publications on the Web when KG may have different versions.
* Marc RAYNAL, IFV: he provides the sources for grapevine phenological scales. He proposes some alignements between grapevine scales.
* Catherine ROUSSEY, MISTEA, INRAE (https://orcid.org/0000-0002-3076-5499): she proposes a first draft of conceptualization for PPDO. She creates the OWL files for PPDO and associate KGs using Protege tool and cellfie plugin. She updates the description of PPDO in Agroportal repository.
* François-Xavier SENNESAL, Dipso, INRAE (https://orcid.org/0000-0003-2487-1743): he publishes the ontology PPDO on the Web.
* Anne TIREAU, MISTEA, INRAE (https://orcid.org/0000-0001-8501-6922): she builds the XLSX files for BBCH individual phenological scales: stone fruit arboriculture, pome fruit arboriculture.

 
## Documentation

PPDO is described in two publications:
* C. ROUSSEY, X. DELPUECH, F. AMARDEILH, S. BERNARD, C. JONQUET. Semantic Description of Plant Phenological Development Stages, starting with Grapevine. In Proceedings of 14th international conference on Metadata and Semantics Research Conference (MTSR), Madrid, Spain, 2-4 December 2020.   In: Garoufallou E., Ovalle-Perandones MA. (eds) Metadata and Semantic Research. MTSR 2020. Communications in Computer and Information Science (CCIS), vol 1355. Springer, Cham, p 257-268.  https://dx.doi.org/10.1007/978-3-030-71903-6_25, https://hal.inrae.fr/hal-02996846, OA 
* C. ROUSSEY,  X. DELPUECH, M. RAYNAL, F. AMARDEILH, S. BERNARD, C. JONQUET, C. NOÛS. Description sémantique des stades de développement phénologique des plantes, cas d’étude de la vigne. Dans les Actes des 32e  Journées Francophones d'Ingénierie des Connaissances IC 2021, adossée à la 14e Plate-forme Francophone d'Intelligence Artificielle, 28 juin - 2 juillet 2021, Bordeaux, France. https://hal.inrae.fr/emse-03260085, OA
 
# Git content description

ppdo_ontology_latest.owl is the last version of ppdo in OWL/XML format to use with Protégé.

ppd_general_latest.owl is the last version of the two generic scales.
ppd_general_latest.rdf is the RDF-XML format handled by Skos Play tool.

Each BBCH individual scale has its own folder.


## Phenological Scales of Grapevine

The folder grapevine contains the file that describe the phenological scales of grapevine based on PPDO.

This KG contains the 5 aligned scales of grapevine phenological stages. This work was made under the supervision of Xavier DELPUECH and Marc RAYNAL from French Institute of Grapevine and Vine (IFV).

## Phenological Scales of Cereals

The folder cereals contains the file that describe the phenological BBCH scale of wheat, barley, oat, rye based on PPDO.
This KG was made under the supervision of Matthieu HIRSCHY, ACTA.


## Phenological Scales of Root and Stem Vegetables

The folder rootAndStemVegetables contains the file that describe the phenological scale of carrot, celeriac, kohlrabi, chicory, radish, swede, scorzonera based on PPDO.
This KG was made under the supervision of Florence AMARDEILH, Elzeard.

## Phenological Scales of Leaf Vegetables forming Heads

The folder leafVegetablesFormingHeads contains the file that describe the phenological scale of cabbage, chinese cabbage, lettuce, endive based on PPDO.
This dataset was made under the supervision of Florence AMARDEILH, Elzeard.

## Phenological Scales of Solanaceous Fruits

The folder solanaceousFruits contains the file that describe the phenological scale of tomato, aubergines, paprika and eggplant based on PPDO.
This dataset was made under the supervision of Florence AMARDEILH, Elzeard.

## Phenological Scales of Tree (Arboriculture)

This dataset was made under the supervision of Anne TIREAU, Mistea INRAE.





