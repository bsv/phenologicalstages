# Plant Phenological Development knowledge graph dedicated to POme Fruit (PPD-POF)

The Knowledge Graph about Plant Phenological Development dedicated to POme Fruit (PPD-POF) contains the description of scale for apple tree and pear tree.

The KG contains the phenological scales:
* BBCH individual scale of pome fruit.

This dataset is an output of D2KAB project task 4.3. 

## Access Description and Identifier

The URI of PPD-POF is https://opendata.inrae.fr/ppd-res/pomeFruit

PPD-POF is under licence https://creativecommons.org/licenses/by-nd/4.0/
If you want to modify this KG please contact Catherine ROUSSEY <catherine.roussey@inrae.fr>

PPD-POF is available on :
 * AgroPortal  https://agroportal.lirmm.fr/ontologies/PPD-POF
 * SPARQL end point  https://rdf.codex.cati.inrae.fr/ppd-res/pomeFruit/sparql

## Development Method
The KG populates the ontology PPDO using the stages of one scale.
The population process was made using Protégé Cellfie plugin.
All the files used during the generation of pomeFruit_scale_latest.owl are stored in cellfie directory.

## Sources

* Growth stages of mono- and dicotyledonous plants BBCH Monograph edited by Uwe Meier Julius Kühn-Institut (JKI) Quedlinburg 2018 
DOI: 10.5073/20180906-074619
ISBN: 978-3-95547-071-5
* Stades phénologiques des mono-et dicotylédones cultivées BBCH
Monographie rédigé par Uwe Meier Instituto Julius Kühn (JKI, siglas en alomán) Quedlinburg, 2018 
DOI: 10.5073/20180906-075455
ISBN: 978-3-95547-072-2
* Estadios de las plantas mono-y dicotyledóneas BBCH Monografia Elaborado por Uwe Meier Instituto Julius Kühn (JKI, siglas en alomán) Quedlinburg, 2018 
DOI: 10.5073/20180906-075743
ISBN: 978-3-95547-069-2
* Entwicklungsstadien mono- und dikotyler Pflanzen BBCH Monografie bearbeitet von Uwe Meier Julius Kühn-Institut (JKI) Quedlinburg 2018 DOI: 10.5073/20180906-075119
ISBN: 978-3-95547-070-8
* BLOESCH, Bernard et VIRET, Olivier. Stades phénologiques repères des fruits à pépins (pommier et poirier). Revue suisse de viticulture, arboriculture et horticulture, 2013, vol. 45, no 2, p. 128-131.

## Contributors
* Evelyne COSTES, AGAP, INRAE (https://orcid.org/0000-0002-4848-2745): she provides some explanations about the stages untitled mouse-ear that appear two times in the BBCH individual scale of pome fruit.
* Isabelle FARRERA, AGAP, SUPAGRO, (https://orcid.org/0000-0002-1210-5032): she validates the BBCH individual scale of pome fruit.
* Stephan BERNARD, LISC, INRAE (https://orcid.org/0000-0001-9694-1443): he publishes on the web the phenological scales per crops and takes care of the associated SPARQL endpoints.
* Catherine ROUSSEY, MISTEA, INRAE (https://orcid.org/0000-0002-3076-5499): she proposes a first draft of conceptualization for PPDO. She creates the OWL files for PPDO and associate KGs using Protege tool and cellfie plugin. She updates the description of pome fruit scale Knowledge Graph in Agroportal repository.
* Anne TIREAU, MISTEA, INRAE (https://orcid.org/0000-0001-8501-6922):  She builds the XLSX files for BBCH phenological scales: pome fruit.

Productors:
* National research institute for agriculture food and environment (INRAE) https://www.inrae.fr/, ror:https://ror.org/003vg9w96
* Mathématiques, Informatique et Statistique pour l'Environnement et l'Agronomie (INRAE) (MISTEA) https://www6.montpellier.inra.fr/mistea_eng/, ror: https://ror.org/01pd2sz18

Funding:
* ANR-18-CE23-0017 D2KAB (Data to Knowledge in Agronomy and Biodiversity) http://www.d2kab.org . WP T4.3 BSV Reader

## Git content description

pomeFruit_scale_latest.owl is the last version of pome fruit scale knowledge graph.
pomeFruit_scale_latest.rdf is the RDF-XML format handle by SkosPlay tool.

## Modifications
note the stage untittled "mouse-ear" is ambiguous and used as label of two stages. Thus we decide to find new labels.


