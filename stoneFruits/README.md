# Plant Phenological Development knowledge graph dedicated to STone Fruits (PPD-STF)

The Knowledge Graph about Plant Phenological Development dedicated to STone Fruits (PPD-STF) contains the description of scale for cherry tree, plum tree, peach tree and apricot tree.

The KG contains the phenological scales:
* BBCH individual scale of stone fruit.

This dataset is an output of D2KAB project task 4.3. 

## Access Description and Identifier

The URI of PPD-STF is  https://opendata.inrae.fr/ppd-res/stoneFruit# 

PPD-STF is under licence https://creativecommons.org/licenses/by-nd/4.0/
If you want to modify this KG please contact Catherine ROUSSEY <catherine.roussey@inrae.fr>

PPD-STF is available on :
 * AgroPortal  https://AgroPortal.lirmm.fr/ontologies/PPD-STF
 * SPARQL end point   https://rdf.codex.cati.inrae.fr/ppd-res/stoneFruit/sparql

## Development Method
The KG populates the ontology PPDO using the stages of one scale.
The population process was made using Protégé Cellfie plugin.
All the files used during the generation of stoneFruit_scale_latest.owl are stored in cellfie directory.

## Sources

* Growth stages of mono- and dicotyledonous plants BBCH Monograph edited by Uwe Meier Julius Kühn-Institut (JKI) Quedlinburg 2018 
DOI: 10.5073/20180906-074619
ISBN: 978-3-95547-071-5
* Stades phénologiques des mono-et dicotylédones cultivées BBCH
Monographie rédigé par Uwe Meier Instituto Julius Kühn (JKI, siglas en alomán) Quedlinburg, 2018 
DOI: 10.5073/20180906-075455
ISBN: 978-3-95547-072-2
* Estadios de las plantas mono-y dicotyledóneas BBCH Monografia Elaborado por Uwe Meier Instituto Julius Kühn (JKI, siglas en alomán) Quedlinburg, 2018 
DOI: 10.5073/20180906-075743
ISBN: 978-3-95547-069-2
* Entwicklungsstadien mono- und dikotyler Pflanzen BBCH Monografie bearbeitet von Uwe Meier Julius Kühn-Institut (JKI) Quedlinburg 2018 DOI: 10.5073/20180906-075119
ISBN: 978-3-95547-070-8

## Contributors

* Isabelle FARRERA, AGAP, SUPAGRO, (https://orcid.org/0000-0002-1210-5032): she validates the BBCH individual scale of stone fruit.
* Stephan BERNARD, LISC, INRAE (https://orcid.org/0000-0001-9694-1443): he publishes on the web the phenological scales per crops and takes care of the associated SPARQL endpoints.
* Catherine ROUSSEY, MISTEA, INRAE (https://orcid.org/0000-0002-3076-5499): she proposes a first draft of conceptualization for PPDO. She creates the OWL files for PPDO and associate KGs using Protege tool and cellfie plugin. She updates the description of stone fruit scale Knowledge Graph in Agroportal repository.
* Anne TIREAU, MISTEA, INRAE (https://orcid.org/0000-0001-8501-6922):  She builds the XLSX files for BBCH phenological scales: stone fruit.

Productors:
* National research institute for agriculture food and environment (INRAE) https://www.inrae.fr/, ror:https://ror.org/003vg9w96
* Mathématiques, Informatique et Statistique pour l'Environnement et l'Agronomie (INRAE) (MISTEA) https://www6.montpellier.inra.fr/mistea_eng/, ror: https://ror.org/01pd2sz18

Funding:
* ANR-18-CE23-0017 D2KAB (Data to Knowledge in Agronomy and Biodiversity) http://www.d2kab.org . WP T4.3 BSV Reader

## Git content description

stoneFruit_scale_latest.owl is the last version of stone fruit scale knowledge graph.
stoneFruit_scale_latest.rdf is the format handle by SkosPlay tool.

## Modifications


