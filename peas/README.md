# Plant Phenological Development knowledge graph dedicated to pea (PPD-PEA)

The Knowledge Graph on Plant Phenological Development of Pea (PPD-PEA) contains the RDF description of BBCH individual scale of pea (Pisum sativum L).

The KG contains the phenological scales:
* BBCH individual scale of pea.

This dataset is an output of STAR project. 

## Access Description and Identifier

The URI of PPD-PEA is  https://opendata.inrae.fr/ppd-res/pea 

PPD-PEA is under licence https://creativecommons.org/licenses/by-nd/4.0/ 
If you want to modify this KG please contact Catherine ROUSSEY <catherine.roussey@inrae.fr>

PPD-PEA is available on :
 * AgroPortal  https://AgroPortal.lirmm.fr/ontologies/PPD-PEA
 * SPARQL end point    https://opendata.inrae.fr/ppd-res/pea/sparql

## Development Method
The KG populates the ontology PPDO using the stages of one scale.
The population process was made using Protégé Cellfie plugin.
All the files used during the generation of pea_scale_latest.owl are stored in cellfie directory.

## Sources

* Growth stages of mono- and dicotyledonous plants BBCH Monograph edited by Uwe Meier Julius Kühn-Institut (JKI) Quedlinburg 2018 
DOI: 10.5073/20180906-074619
ISBN: 978-3-95547-071-5
* Stades phénologiques des mono-et dicotylédones cultivées BBCH
Monographie rédigé par Uwe Meier Instituto Julius Kühn (JKI, siglas en alomán) Quedlinburg, 2018 
DOI: 10.5073/20180906-075455
ISBN: 978-3-95547-072-2
* Estadios de las plantas mono-y dicotyledóneas BBCH Monografia Elaborado por Uwe Meier Instituto Julius Kühn (JKI, siglas en alomán) Quedlinburg, 2018 
DOI: 10.5073/20180906-075743
ISBN: 978-3-95547-069-2
* Entwicklungsstadien mono- und dikotyler Pflanzen BBCH Monografie bearbeitet von Uwe Meier Julius Kühn-Institut (JKI) Quedlinburg 2018 DOI: 10.5073/20180906-075119
ISBN: 978-3-95547-070-8

## Contributors
* Florence AMARDEILH, ELZEARD, (https://orcid.org/0000-0002-6306-4437):  She builds the XLSX files for BBCH phenological scales: BBCH individual scale of pea.
* Stephan BERNARD, LISC, INRAE (https://orcid.org/0000-0001-9694-1443): he publishes on the web the phenological scales per crops and takes care of the associated SPARQL endpoints.
* Catherine ROUSSEY, MISTEA, INRAE (https://orcid.org/0000-0002-3076-5499): she proposes a first draft of conceptualization for PPDO. She creates the OWL files for PPDO and associate KGs using Protege tool and cellfie plugin. She updates the description of stone fruit scale Knowledge Graph in Agroportal repository.


Productors:
* National research institute for agriculture food and environment (INRAE) https://www.inrae.fr/, ror:https://ror.org/003vg9w96
* Elzeard https://www.elzeard.co/


Funding:
French National project CASDAR STAR


## Git content description

pea_scale_latest.owl is the last version of stone fruit scale knowledge graph.
pea_scale_latest.rdf is the format handle by SkosPlay tool.

## Modifications


