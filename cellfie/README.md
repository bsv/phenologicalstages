# Cellfie transformation rules

All the files enable to create the ppdo.owl file.

be sure that the preferences of protege is fixed to
* renderer : Render by entity IRI short name
* new entities: active ontology IRI and separator #


ppdo_vide.owl is the entry file used during cellfie transformation rules in order to produce ppdo_ontology_latest.owl.
change the dcterms:issued date to today. 


ppdgen_vide.owl is the entry file during the cellfie transformation rules in order to produce ppd_generic_latest.owl.

bbch_principalStages.xlsx contains the information about principal stages of BBCH general scale and BBCH global scale.

rulesClassGeneration_bbch_principalStages.json is the cellfie rules to generate ppdo Classes. 
It takes as input bbch_principalStages.xlsx files.

rulesInstanceGeneration_bbch_generalScale_principalStages.json is the cellfie rules to generate individuals that represent the principal stages of BBCH general scale. 
It takes as input bbch_principalStages.xlsx files.

rulesInstanceGeneration_bbch_globalScale_principalStages.json is the cellfie rules to generate individuals that represent the principal stages of BBCH global scale. 
It takes as input bbch_principalStages.xlsx files.

bbch_global_secondaryStages.xlsx contains the information about secondary stages of BBCH global scale.

rulesClassGeneration_bbch_secondaryStages.json is the cellfie rules to generate ppdo Classes. 
It takes as input bbch_global_secondaryStages.xlsx files.

rulesInstanceGeneration_bbch_globalScale_secondaryStages.json is the cellfie rules to generate individuals that represent the secondary stages of BBCH global scale. 
It takes as input bbch_global_secondaryStages.xlsx files.

bbch_general_secondaryStages.xlsx contains the information about secondary stages of BBCH general scale.

rulesInstanceGeneration_bbch_generalScale_secondaryStages.json is the cellfie rules to generate individuals that represent the secondary stages of BBCH general scale. 
It takes as input bbch_general_secondaryStages.xlsx files.





