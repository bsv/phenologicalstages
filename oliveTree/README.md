# Plant Phenological Development knowledge graph dedicated to OLive Tree (PPD-OLT)

The Knowledge Graph about Plant Phenological Development dedicated to Olive tree contains one scale.

The KG contains the phenological scale:
* BBCH individual scale of olive tree

This dataset is an output of D2KAB project task 4.3. 

## Access Description and Identifier

The URI of PPD-OLT is https://opendata.inrae.fr/ppd-res/oliveTree#  

PPD-OLT is under licence https://creativecommons.org/licenses/by-nd/4.0/
Please contact Catherine ROUSSEY from INRAE <catherine.roussey@inrae.fr> if you want to modify this file.

PPD-OLT is available on :
 * AgroPortal  https://agroportal.lirmm.fr/ontologies/PPD-OLT
 * Research Data Gouv: ROUSSEY, Catherine; AZIZAN, Amira, 2024, "phenological scales of olive tree", https://doi.org/10.57745/E33CXX, Recherche Data Gouv, V1
 * SPARQL end point https://rdf.codex.cati.inrae.fr/ppd-res/oliveTree/sparql

## Development Method
The KG populates the ontology PPDO using the stages of one scale.
The population process was made using Protégé Cellfie plugin.
All the files used during the generation of oliveTree_scale_latest.owl are stored in cellfie directory.

## Sources

* Growth stages of mono- and dicotyledonous plants BBCH Monograph edited by Uwe Meier Julius Kühn-Institut (JKI) Quedlinburg 2001
* Stades phénologiques des mono-et dicotylédones cultivées BBCH
Monographie rédigé par Uwe Meier Instituto Julius Kühn (JKI, siglas en alomán) Quedlinburg, 2001
* Estadios de las plantas mono-y dicotyledóneas BBCH Monografia Elaborado por Uwe Meier Instituto Julius Kühn (JKI, siglas en alomán) Quedlinburg, 2001
* Entwicklungsstadien mono- und dikotyler Pflanzen BBCH Monografie bearbeitet von Uwe Meier Julius Kühn-Institut (JKI) Quedlinburg 2001
* Sanz‐Cortés, F., Martinez‐Calvo, J., Badenes, M. L., Bleiholder, H., Hack, H., Llácer, G., & Meier, U. "Phenological growth stages of olive trees (Olea europaea)". Annals of applied biology, 2002, vol 140, no 2, pp 151-157.  https://doi.org/10.1111/j.1744-7348.2002.tb00167.x

## Contributors
* Stephan BERNARD, LISC, INRAE (https://orcid.org/0000-0001-9694-1443): he publishes on the web the phenological scales per crops and takes care of the associated SPARQL endpoints.
* Catherine ROUSSEY, MISTEA, INRAE (https://orcid.org/0000-0002-3076-5499): she proposes a first draft of conceptualization for PPDO. She creates the OWL files for PPDO and associate KGs using Protege tool and cellfie plugin. She updates the description of stone fruit scale Knowledge Graph in Agroportal repository.
* Amira AZIZAN, CEFE, CNRS, (https://orcid.org/0000-0001-9479-0581): she reviews the RDF representation of BBCH scale for olive tree. She detects errors in the english version of this RDF representation.

Productors:
* National research institute for agriculture food and environment (INRAE) https://www.inrae.fr/, ror:https://ror.org/003vg9w96
* Mathématiques, Informatique et Statistique pour l'Environnement et l'Agronomie (INRAE) (MISTEA) https://www6.montpellier.inra.fr/mistea_eng/, ror: https://ror.org/01pd2sz18

Funding:
* ANR-18-CE23-0017 D2KAB (Data to Knowledge in Agronomy and Biodiversity) http://www.d2kab.org . WP T4.3 BSV Reader

## Git content description

oliveTree_scale_latest.owl is the last version of olive tree scale knowledge graph.
oliveTree_scale_latest.rdf is the format handle by SkosPlay tool

## Modifications
There are some errors in the BBCH monograph in French and German that were corrected during the RDF KG generation. the stage 19 was forgotten in those versions. Moreover the stages 57 and 59 have the same label and definitions.




